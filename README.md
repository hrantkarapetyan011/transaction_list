# Transaction List

Terms of reference - transaction list

# Demo
https://drive.google.com/file/d/1QNS3c4s8zF2NGPztm5Ynb39FZlYputYH/view?usp=sharing

# Important info

- Free version of Firebase doesn't return token, that's why I use UID instead of token

- We have login and signup screens with forms and validations and logout functionality in settings screen

- Transactions are not stored in db, I use static list, so for each user we will have the same list
 
- To login, first you should get credentials. You can do it in signup form.

# Configs

- Flutter version 3.13.6
- Dart version 3.1.3
- DevTools version 2.25.0


### Folder structure

When creating an architecture, we must use Clean Architecture.

```
lib
├── config
│   ├── utils 
│   │    └── utils.dart
│   ├── colors.dart 
│   ├── constants.dart  
│   ├── fonts.dart  
│   ├── images.dart  
│   └── strings.dart  
│
├── data                                     # this layer is responsible for communicating with the back-end and data management logic
│   ├── sources                           
│   │   ├── remote_sources                   # here we store all classes for remote data storage
│   │   │    └── firabase/supabase.dart
│   │   └── local_sources                    # here we store all classes for local data storage
│   │        └── shared_preferences.dart
│   │        └── secure_storage.dart
│   │        └── db.dart
│   ├── exception.dart
│   ├── models                               # here we store the models that are used in the presentation
│   │   └── user_model.dart
│   └── repositories                         # takes data from the API and passes use cases
│       └── user_repository_impl.dart        # this is a concrete implementation of the repository
├── domain                                   # this layer is responsible for the business logic.
│   ├── entities                             # here we store the models that come from the backend
│   │   └── user.dart
│   ├── repositories                         # abstract repositories are stored here
│   │   └── user_repository.dart             # this is an abstract kind of repository
│   └── usecases                             # it has the same functionality as services
│       └── get_current_user.dart
├── presentation                             # there is no business logic processing at this level, so it is only used for displaying the user interface and handling events
│   ├── provider/bloc/riverpod/redux         # here we store tools for state management
│   │   └── user_bloc.dart  
│   ├── widgets
│   │   └── user_widget.dart
│   ├── screens
│   │   └── user_screen.dart
│   ├── themes.dart
│   └── routes.dart
├── injection.dart                           # get_it IoC locator (Dependency_injection)
├── app_manager.dart
└── main.dart


```

![](https://miro.medium.com/max/1400/1*62vHxhRxY05gbvZW5UrvOQ.png)

#### domain

This is the most important part of the application and is the first layer we need to develop. Because we
we are designing a client application, we already have some initial data (which we get from
back-end), so we need to take them into account when designing our domain objects.
However, this does not mean that we should use the same data format that we receive from
server part. Our application has its own business logic, so we must define entities,
who are involved in this process. Domain is the core of our application - business logic, here we have
there must be abstract entities, and already in data we give their implementation.

The domain layer is the deepest in a clean architecture. This level contains code for
business logic applications such as entities and use cases. A level that does not depend on any
other levels, here is only the domain level (independent), which is
business logic code. Thus, the application becomes more adaptive and dynamic.

#### data

This part represents the data layer of the application. The data module, which is part of the
external layer, responsible for data retrieval. This may be in the form of API calls to the server and/or
local database. It also contains repository implementations.

#### presentation

This is where the user interface goes. We need widgets to display something on
screen. These widgets are state controlled using various design patterns and state
management - in our case BloC. Most of the UI code should be
divided and organized according to some kind of logic.

#### assets

All fonts, pictures, media files in the project are stored in the `/assets/` folder.

```
project_name
    └── assets                          
         ├── fonts                       
         ├── images
         └── media
```

#### UI

As already mentioned above, [presentation](#presentation) is the part where the custom
interface. In the presentation folder, there are two folders `widgets` and `screens` that are directly related to
user interface.

##### widgets

The `widgets` folder is for a shared project. We create global widgets in it, which
concern the entire UI. You need to take and configure each widget for its specific implementation (specific
screen).

For example:

```
 presentation                                                       
    └── widgets
         ├── tl_button.dart                       
         ├── tl_text.dart
         ├── tl_appbar-with_back.dart
         └── tl_text_field.dart
```

##### screens

The `screens` folder is intended for individual-specific screens of the general project. In it we create
specific folders with the names of specific screens, and in them we create a file for this specific screen
and a `widgets` folder, which is only for the widgets of this particular screen.

For example:

```
 presentation                                                       
    └── screens
         ├── login 
         │        ├── login_screen.dart
         │        └── widgets
         │               ├── forgot_password.dart                       
         │               ├── login_form.dart                                            
         │               └── login_password.dart                                    
         ├── signup  
         │     ├── signup_screen.dart
         │     └── widgets
         │            ├── signup_email-input.dart                       
         │            ├── signup_form.dart                                            
         │            └── signup_name_input.dart                   
         └── transaction_diagram
               ├── transaction_diagram_screen.dart
               └── widgets
                      ├── transaction_diagram.dart  
                      └── transaction_list_tile.dart  
                          
```




