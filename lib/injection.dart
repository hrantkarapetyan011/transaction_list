import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';

import 'data/sources/local_sources/secure_storage_manager.dart';

/// Created by HrAnt
/// Date: 16.06.23

/// GetIt instance
final locator = GetIt.instance;

Future<void> initLocator() async {
  locator.registerSingleton<SecureStorageManager>(
    SecureStorageManager(
      secureStorage: const FlutterSecureStorage(),
    ),
  );
}

/// registerFactory: Will register a type so that a new instance will be created on each call.
///
/// registerFactoryParam: It is the same as registerFactory, but the only difference is that you can pass up to 2 parameters.
///
/// registerSingleton: Will registers a type as Singleton, which means that on each call the same instance will always be returned during the app lifecycle.
///
/// registerLazySingleton: It is the same as registerSingleton, but the only difference is that in this case the singleton is registered only when it’s requested as a dependency for some other class.
