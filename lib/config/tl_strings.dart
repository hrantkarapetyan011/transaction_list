/// Created by HrAnt
/// Date: 16.06.23

/// Base Url
const String baseUrl = 'https://dev.api.transaction_list/v1';

/// AUTH Endpoints
const String authRegister = '/auth/register';
const String authLogin = '/auth/login';

/// API Keys
const String authorization = 'Authorization';

/// Button names
const String createAccountBtn = 'Create account';
const String gotAnAccount = 'Got an account? Log in';
const String signUpBtn = 'Sign up';
const String loginBtn = 'Login';
const String forgotPasswordBtn = 'Forgot password?';
const String cancelBtn = 'Cancel';
const String cancelTransactionBtn = 'Cancel Transaction';
const String logoutBtn = 'Log out';

/// Privacy policy and terms & conditions
const String privacy = 'By continuing, you agree to our\t';
const String termsRich = 'Terms and Conditions\t';
const String and = 'and\t';
const String privacyRich = 'Privacy Policy';

/// Appbars titles
const String signUpTitle = 'Sign up';
const String logInTitle = 'Log in';
const String forgotPasswordTitle = 'Forgot password';
const String transactionListTitle = 'Transaction List';
const String transactionDiagramTitle = 'Transaction Diagram';
const String transactionNoFoundTitle = 'No Transactions Found';
const String settingsTitle = 'Settings';
const String transactionInfoTitle = 'Transaction';

/// Toasts
const String signupSuccessfully = 'Signup successfully';
const String transactionsDeletedSuccessfully =
    'Transaction Successfully Canceled';

/// TextFields hints
const String nameHint = 'Name';
const String emailHint = 'Email';
const String passwordHint = 'Password';
const String newPasswordHint = 'New password';
const String newPasswordRepeatHint = 'Repeat password';

/// Transaction types
const String translationType = 'Translation';
const String replenishmentType = 'Replenishment';
const String withdrawalType = 'Withdrawal';

/// Transaction cart
const String number = 'Transaction number';
const String amount = 'Amount';

/// Login Screen Descriptions
const String loginScreenTitle = 'Welcome back';
const String loginScreenSubtitle = 'Log in using your email.';

/// Signup Screen Descriptions
const String signupScreenTitle = 'Create an account';
const String signupScreenSubtitle =
    'Your password shall be a minimum of 8 symbols, containing at least one letter and number.';

/// Settings Screen
const String logOutDescription = 'Are you sure you want to logout?';

/// ERROR texts
const String somethingWentWrong = 'Something went wrong';
const String loginError = 'You have entered an invalid username or password';

/// Shared keys
const String accessToken = 'token';
