/// Created by HrAnt
/// Date: 16.06.23

/// TransactionList logo
const String logo = 'assets/images/logo.jpeg';

/// Welcome Screen
const String welcome = 'assets/images/welcome.png';

/// SVG/PNG ICONS
const String checkboxIcon = 'assets/icons/icon_checkbox.svg';
const String checkboxCheckedIcon = 'assets/icons/icon_checkbox_checked.svg';

const String eyeIcon = 'assets/icons/icon_eye.svg';
const String eyeInvisibleIcon = 'assets/icons/icon_eye_invisible.svg';

const String transactionListIcon = 'assets/icons/icon_transaction_list.png';
const String transactionDiagramIcon =
    'assets/icons/icon_transaction_diagram.png';

const String settingsIcon = 'assets/icons/icon_settings.svg';
