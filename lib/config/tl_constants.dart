import 'package:flutter/cupertino.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';
import '/config/utils.dart';
import '/data/enums.dart';
import '/domain/entities/bank/transaction_info.dart';
import '/presentation/screens/transaction_diagram/transaction_diagram_screen.dart';
import '/presentation/screens/transaction_list/transaction_list_screen.dart';

/// Created by HrAnt
/// Date: 16.06.23

/// RegExp
/// ^                                            Match the beginning of the string
/// (?=.*[0-9])                                  Require that at least one digit appear anywhere in the string
/// (?=.*[a-z])                                  Require that at least one lowercase letter appear anywhere in the string
/// (?=.*[A-Z])                                  Require that at least one uppercase letter appear anywhere in the string
/// (?=.*[!#%*.&()])                             Require that at least one special character appear anywhere in the string
/// .{8,?}                                       The password must be at least 8 characters long, but no more than ?
/// $
///                                              Match the end of the string.

const String passwordRegexMinimumEightCharactersAtLeastOneLetterAndOneNumber =
    r'(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$';
RegExp passwordRegex =
    RegExp(passwordRegexMinimumEightCharactersAtLeastOneLetterAndOneNumber);

const String emailRegexExample =
    r'^.+@[A-Za-z0-9]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$';
RegExp emailRegex = RegExp(emailRegexExample);

/// Font Sizes
const double headerLarge = 40.0;
const double headerMedium = 32.0;
const double headerSmall = 16.0;
const double paddingBottom = 44.0;

/// Bottom nav bar screens
const List<Widget> pages = <Widget>[
  TransactionListScreen(),
  TransactionDiagramScreen(),
];

/// Home Screen Appbar Titles
const List<String> titles = <String>[
  transactionListTitle,
  transactionDiagramTitle,
];

/// Transactions
List<TransactionInfoEntity> transactions = [
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 100.0,
    number: 1223,
    commission: 1.0,
    total: 101.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 220.0,
    number: 1224,
    commission: 2.0,
    total: 222.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.withdrawal,
    amount: 330.0,
    number: 1225,
    commission: 3.0,
    total: 333.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.replenishment,
    amount: 4000.0,
    number: 1226,
    commission: 6.0,
    total: 4006.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 900.0,
    number: 1227,
    commission: 4.0,
    total: 904.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.withdrawal,
    amount: 745.0,
    number: 1228,
    commission: 3.0,
    total: 747.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 37.0,
    number: 1229,
    commission: 1.0,
    total: 38.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 2300.0,
    number: 1230,
    commission: 20.0,
    total: 2320.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 490.0,
    number: 1231,
    commission: 2.0,
    total: 492.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.replenishment,
    amount: 759.0,
    number: 1232,
    commission: 3.0,
    total: 762.0,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.translation,
    amount: 6.0,
    number: 1233,
    commission: 0.5,
    total: 6.5,
    date: randomDate(1),
  ),
  TransactionInfoEntity(
    type: TransactionType.withdrawal,
    amount: 650.0,
    number: 1234,
    commission: 3,
    total: 653.0,
    date: randomDate(1),
  ),
];

/// Transaction Diagram Wheel
const List<Color> diagramColors = [lightBlue, lightGreen, grape];
const List<String> transactionsTypes = [
  translationType,
  replenishmentType,
  withdrawalType
];
