import 'dart:math';

import 'package:flutter_native_splash/cli_commands.dart';
import 'package:intl/intl.dart';

/// Created by HrAnt
/// Date: 16.06.23

String dateFormat(String? date) {
  return date != null
      ? DateFormat('MMM d, h:mm a').format(DateTime.parse(date)).capitalize()
      : '??/??/??? - ?? : ??';
}

String randomDate(int week) {
  Random gen = Random();
  int range = week * 7;

  DateTime randomDate = DateTime.now().add(
    Duration(
      days: gen.nextInt(range) * -1,
      hours: gen.nextInt(12) * -1,
      minutes: gen.nextInt(60) * -1,
    ),
  );

  return dateFormat(
    randomDate.toString(),
  );
}
