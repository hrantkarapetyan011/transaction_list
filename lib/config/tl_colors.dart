import 'package:flutter/material.dart';

/// Created by HrAnt
/// Date: 16.06.23

const Color charcoal = Color(0xFF2E2A2B);
const Color lollipop = Color(0xFFF16461);
const Color vanillaIce = Color(0xFFEAE7D8);
const Color mint = Color(0xFFBCBCAB);
const Color grape = Color(0xFFA84A58);
const Color lightBlue = Color(0xFF01B5F7);
const Color lightGreen = Color(0xFF5CCB45);
const Color overlayColor = Colors.white10;
