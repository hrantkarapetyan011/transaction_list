import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_images.dart';
import '/presentation/routes.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TLAppbarWithSettings extends StatelessWidget
    implements PreferredSizeWidget {
  final String title;

  const TLAppbarWithSettings({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: true,
      backgroundColor: vanillaIce,
      elevation: 0.0,
      title: TLText(
        text: title,
        fontSize: headerSmall,
        fontWeight: FontWeight.w700,
        color: charcoal,
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () => _fetchUserInfo(context),
            child: SvgPicture.asset(
              settingsIcon,
              height: 22.0,
              width: 22.0,
              fit: BoxFit.scaleDown,
              colorFilter: const ColorFilter.mode(
                charcoal,
                BlendMode.srcIn,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  void _fetchUserInfo(BuildContext context) {
    Navigator.pushNamed(
      context,
      routeSettings,
    );
  }
}
