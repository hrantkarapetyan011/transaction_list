import 'package:flutter/cupertino.dart';

import '/presentation/widgets/tl_loading_indicator.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TLLoadingLayout extends StatelessWidget {
  final bool currentState;
  final Widget child;

  const TLLoadingLayout({
    Key? key,
    required this.child,
    this.currentState = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        currentState ? const TLLoadingIndicator() : const SizedBox(),
      ],
    );
  }
}
