import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_images.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLTextField extends StatefulWidget {
  final String name;
  final String hint;
  final int? maxLines;
  final int? minLines;
  final bool obscureText;
  final bool suffixIconVisible;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final TextEditingController controller;
  final FormFieldValidator? validator;
  final void Function(String?)? onChanged;

  const TLTextField({
    Key? key,
    required this.name,
    required this.hint,
    required this.keyboardType,
    required this.textInputAction,
    required this.controller,
    this.validator,
    this.onChanged,
    this.maxLines = 1,
    this.minLines = 1,
    this.obscureText = false,
    this.suffixIconVisible = false,
  }) : super(key: key);

  @override
  State<TLTextField> createState() => _TLTextFieldState();
}

class _TLTextFieldState extends State<TLTextField> {
  bool _passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: FormBuilderTextField(
        controller: widget.controller,
        validator: widget.validator,
        onChanged: widget.onChanged,
        name: widget.name,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        maxLines: widget.maxLines,
        minLines: widget.minLines,
        keyboardType: widget.keyboardType,
        textInputAction: widget.textInputAction,
        obscureText: widget.obscureText && !_passwordVisible,
        obscuringCharacter: '✱',
        style: const TextStyle(
          color: charcoal,
          fontSize: headerSmall,
          fontWeight: FontWeight.w400,
        ),
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.only(top: 16.0, right: 16.0, bottom: 16.0),
          prefix: const Padding(
            padding: EdgeInsets.only(left: 16.0),
          ),
          counterText: "",
          errorMaxLines: 3,
          hintText: widget.hint,
          fillColor: mint,
          errorStyle: const TextStyle(
            fontSize: 14.0,
            color: grape,
            fontWeight: FontWeight.w400,
          ),
          hintStyle: const TextStyle(
            color: charcoal,
            fontSize: headerSmall,
            fontWeight: FontWeight.w400,
          ),
          suffixIcon: widget.suffixIconVisible
              ? Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: GestureDetector(
                    onTap: () => setState(() {
                      _passwordVisible = !_passwordVisible;
                    }),
                    child: _passwordVisible
                        ? SvgPicture.asset(
                            eyeIcon,
                            height: 16.0,
                            width: 22.0,
                            fit: BoxFit.scaleDown,
                            colorFilter: const ColorFilter.mode(
                              charcoal,
                              BlendMode.srcIn,
                            ),
                          )
                        : SvgPicture.asset(
                            eyeInvisibleIcon,
                            height: 16.0,
                            width: 22.0,
                            fit: BoxFit.scaleDown,
                            colorFilter: const ColorFilter.mode(
                              charcoal,
                              BlendMode.srcIn,
                            ),
                          ),
                  ),
                )
              : null,
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                26.0,
              ),
            ),
            borderSide: BorderSide(
              color: mint,
              width: 1.0,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                26.0,
              ),
            ),
            borderSide: BorderSide(
              color: lightBlue,
              width: 1.0,
            ),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                26.0,
              ),
            ),
            borderSide: BorderSide(
              color: grape,
              width: 1.0,
            ),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                26.0,
              ),
            ),
            borderSide: BorderSide(
              color: grape,
              width: 1.0,
            ),
          ),
        ),
      ),
    );
  }
}
