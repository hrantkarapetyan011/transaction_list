import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '/config/tl_colors.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLBackButton extends StatelessWidget {
  final Color backColor;

  const TLBackButton({
    Key? key,
    this.backColor = grape,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
        Navigator.pop(context);
      },
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      child: SizedBox(
        height: 100.0,
        width: 100.0,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: TLText(
              text: 'Back',
              color: backColor,
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}
