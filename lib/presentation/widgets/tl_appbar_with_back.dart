import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/presentation/widgets/tl_back_button.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLAppbarWithBack extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool backAvailability;
  final bool automaticallyImplyLeading;
  final Color color;
  final Color titleColor;

  const TLAppbarWithBack({
    Key? key,
    required this.title,
    this.backAvailability = true,
    this.automaticallyImplyLeading = true,
    this.color = vanillaIce,
    this.titleColor = charcoal,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: automaticallyImplyLeading,
      backgroundColor: color,
      elevation: 0,
      centerTitle: true,
      leadingWidth: 100,
      leading: backAvailability ? const TLBackButton() : null,
      title: TLText(
        text: title,
        color: titleColor,
        fontSize: headerSmall,
        fontWeight: FontWeight.w400,
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
