import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '/config/tl_colors.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

void showToast({
  required BuildContext context,
  required String title,
  Color color = charcoal,
  int seconds = 3,
  double actionWidth = 76.0,
}) {
  FToast fToast = FToast();
  fToast.init(context);
  fToast.showToast(
    positionedToastBuilder: (context, child) {
      return Positioned(
        top: 0.0,
        left: 0.0,
        child: child,
      );
    },
    toastDuration: Duration(seconds: seconds),
    child: Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 38.0),
        child: Container(
          width: MediaQuery.of(context).size.width - 20.0,
          padding: const EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 16.0),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Row(
            children: [
              Expanded(
                child: TLText(
                  text: title,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w400,
                  color: vanillaIce,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
