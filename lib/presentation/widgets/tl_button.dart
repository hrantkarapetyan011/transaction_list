import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLButton extends StatelessWidget {
  final String name;
  final double width;
  final double paddingBottom;
  final Color btnColor;
  final Color textColor;
  final Color borderSideColor;
  final Color overlayColor;
  final VoidCallback function;

  const TLButton({
    Key? key,
    required this.name,
    required this.function,
    required this.width,
    this.paddingBottom = 0.0,
    this.textColor = vanillaIce,
    this.btnColor = grape,
    this.borderSideColor = grape,
    this.overlayColor = Colors.white10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: paddingBottom),
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          overlayColor: MaterialStateProperty.all(
            overlayColor,
          ),
          backgroundColor: MaterialStateProperty.all(
            btnColor,
          ),
          elevation: MaterialStateProperty.all(0.0),
          side: MaterialStateProperty.all(
            BorderSide(
              width: 1.0,
              color: borderSideColor,
            ),
          ),
          fixedSize: MaterialStateProperty.all(
            Size(
              width,
              52.0,
            ),
          ),
        ),
        onPressed: function,
        child: TLText(
          text: name,
          fontSize: 16.0,
          color: textColor,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
