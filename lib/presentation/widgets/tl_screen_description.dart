import 'package:flutter/cupertino.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_fonts.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLScreenDescription extends StatelessWidget {
  final String title;
  final String subtitle;
  final String fontFamily;
  final int subtitleMaxLines;
  final double titleFontSize;

  const TLScreenDescription({
    Key? key,
    required this.title,
    required this.subtitle,
    this.subtitleMaxLines = 5,
    this.titleFontSize = headerMedium,
    this.fontFamily = sfProDisplayRegular,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TLText(
            text: title,
            textAlign: TextAlign.start,
            fontSize: titleFontSize,
            fontWeight: FontWeight.w500,
            fontFamily: fontFamily,
            color: charcoal,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10.0,
            ),
            child: TLText(
              text: subtitle,
              textAlign: TextAlign.start,
              fontSize: headerSmall,
              fontWeight: FontWeight.w400,
              color: charcoal,
              maxLines: subtitleMaxLines,
            ),
          ),
        ],
      ),
    );
  }
}
