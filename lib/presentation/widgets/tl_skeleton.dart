import 'package:flutter/material.dart';

import '/config/tl_colors.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLSkeleton extends StatelessWidget {
  final String imagePath;
  final int bottomSheetSize;
  final Widget child;

  const TLSkeleton({
    Key? key,
    required this.imagePath,
    required this.child,
    this.bottomSheetSize = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(imagePath),
            filterQuality: FilterQuality.high,
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            const Expanded(
              flex: 7,
              child: SizedBox.expand(),
            ),
            Expanded(
              flex: bottomSheetSize,
              child: Container(
                decoration: const BoxDecoration(
                  color: vanillaIce,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(24),
                    topLeft: Radius.circular(24),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 20.0,
                    right: 20.0,
                    top: 20.0,
                  ),
                  child: child,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
