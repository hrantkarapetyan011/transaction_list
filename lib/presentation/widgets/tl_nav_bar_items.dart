import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_images.dart';

/// Created by HrAnt
/// Date: 18.06.23

List<BottomNavigationBarItem> getBottomNavBarItems() {
  return <BottomNavigationBarItem>[
    /// Transaction List
    BottomNavigationBarItem(
      label: '',
      icon: Image.asset(
        transactionListIcon,
        height: 42.0,
        width: 42.0,
        fit: BoxFit.scaleDown,
        color: mint,
      ),
      activeIcon: Image.asset(
        transactionListIcon,
        height: 42.0,
        width: 42.0,
        fit: BoxFit.scaleDown,
        color: grape,
      ),
    ),

    /// Transaction Diagram
    BottomNavigationBarItem(
      label: '',
      icon: Image.asset(
        transactionDiagramIcon,
        height: 32.0,
        width: 32.0,
        fit: BoxFit.scaleDown,
        color: mint,
      ),
      activeIcon: Image.asset(
        transactionDiagramIcon,
        height: 32.0,
        width: 32.0,
        fit: BoxFit.scaleDown,
        color: grape,
      ),
    ),
  ];
}
