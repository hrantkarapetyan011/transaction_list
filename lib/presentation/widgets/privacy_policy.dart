import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';

/// Created by HrAnt
/// Date: 17.06.23

class PrivacyPolicy extends StatelessWidget {
  const PrivacyPolicy({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: privacy,
        style: const TextStyle(
          color: charcoal,
          fontSize: 16.0,
        ),
        children: <TextSpan>[
          TextSpan(
            text: termsRich,
            recognizer: TapGestureRecognizer()..onTap = () => () {},
            style: const TextStyle(
              color: grape,
              fontWeight: FontWeight.w600,
              fontSize: 16.0,
            ),
          ),
          const TextSpan(
            text: and,
            style: TextStyle(
              color: charcoal,
              fontSize: 16.0,
            ),
          ),
          TextSpan(
            text: privacyRich,
            recognizer: TapGestureRecognizer()..onTap = () => () {},
            style: const TextStyle(
              color: grape,
              fontWeight: FontWeight.w600,
              fontSize: 16.0,
            ),
          ),
        ],
      ),
    );
  }
}
