import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_fonts.dart';

/// Created by HrAnt
/// Date: 17.06.23

class TLText extends StatelessWidget {
  final String text;
  final double fontSize;
  final int maxLines;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final Color color;
  final String fontFamily;

  const TLText({
    Key? key,
    required this.text,
    required this.fontSize,
    required this.fontWeight,
    this.textAlign = TextAlign.center,
    this.color = vanillaIce,
    this.fontFamily = sfProDisplayRegular,
    this.maxLines = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      maxLines: maxLines,
      style: TextStyle(
        fontSize: fontSize,
        color: color,
        fontFamily: fontFamily,
      ),
    );
  }
}
