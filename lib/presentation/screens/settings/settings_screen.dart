import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/data/sources/local_sources/shared_preferences_manager.dart';
import '/domain/usecases/cache_manager/cache_manager_service.dart';
import '/presentation/routes.dart';
import '/presentation/widgets/tl_appbar_with_back.dart';
import '/presentation/widgets/tl_button.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 18.06.23

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: vanillaIce,
      extendBodyBehindAppBar: true,
      appBar: const TLAppbarWithBack(
        title: settingsTitle,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 52.0),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: TLButton(
            btnColor: Colors.transparent,
            textColor: grape,
            name: logoutBtn,
            width: 164.0,
            function: () => _showLogoutSheet(context),
          ),
        ),
      ),
    );
  }

  /// Bottom Sheet with 'Cancel' and 'Log out' buttons.
  void _showLogoutSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      backgroundColor: vanillaIce,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(24.0),
        ),
      ),
      builder: (context) => SizedBox(
        height: 280.0,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 0.0),
              child: TLText(
                text: logOutDescription,
                fontSize: headerMedium,
                fontWeight: FontWeight.w700,
                maxLines: 3,
                color: charcoal,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 28.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TLButton(
                    name: cancelBtn,
                    function: () => Navigator.pop(context),
                    width: 164.0,
                  ),
                  const SizedBox(
                    width: 12.0,
                  ),
                  TLButton(
                    btnColor: Colors.transparent,
                    textColor: grape,
                    name: logoutBtn,
                    function: () => _logout(context),
                    width: 164.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Log Out
  Future<void> _logout(BuildContext context) async {
    await sharedPreferences!.remove(accessToken);
    await clearCache();
    if (mounted) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        routeWelcome,
        (Route<dynamic> route) => false,
      );
    }
  }
}
