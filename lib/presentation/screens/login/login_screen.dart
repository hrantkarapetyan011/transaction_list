import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';
import '/presentation/bloc/login/login_bloc.dart';
import '/presentation/bloc/login/login_state.dart';
import '/presentation/routes.dart';
import '/presentation/screens/login/widgets/login_form.dart';
import '/presentation/widgets/tl_appbar_with_back.dart';
import '/presentation/widgets/tl_loading_layout.dart';
import '/presentation/widgets/tl_screen_description.dart';
import '/presentation/widgets/tl_toast.dart';

/// Created by HrAnt
/// Date: 17.06.23

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccessfullyState) {
          Navigator.pushNamedAndRemoveUntil(
            context,
            routeScreenManager,
            (Route<dynamic> route) => false,
          );
        }
        if (state is LoginFailedState) {
          showToast(
            context: context,
            title: state.error,
            seconds: 3,
            color: lollipop,
          );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return TLLoadingLayout(
            currentState: state is LoadingState,
            child: const Scaffold(
              backgroundColor: vanillaIce,
              resizeToAvoidBottomInset: false,
              appBar: TLAppbarWithBack(
                title: logInTitle,
              ),
              body: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TLScreenDescription(
                      title: loginScreenTitle,
                      subtitle: loginScreenSubtitle,
                    ),
                    LoginForm(),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
