import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';
import '/presentation/routes.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class ForgotPasswordButton extends StatelessWidget {
  const ForgotPasswordButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => Navigator.pushNamed(
        context,
        routeForgotPassword,
      ),
      child: const TLText(
        text: forgotPasswordBtn,
        color: grape,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
      ),
    );
  }
}
