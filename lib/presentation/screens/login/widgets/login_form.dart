import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/domain/entities/auth/login.dart';
import '/presentation/bloc/login/login_bloc.dart';
import '/presentation/bloc/login/login_event.dart';
import '/presentation/widgets/tl_button.dart';
import 'forgot_password_btn.dart';
import 'login_email_input.dart';
import 'login_password_input.dart';

/// Created by HrAnt
/// Date: 17.06.23

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormFormState();
}

class _LoginFormFormState extends State<LoginForm> {
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  Color _btnColor = mint;
  Color _overlayColor = Colors.transparent;

  @override
  void initState() {
    _passwordController.addListener(
      () => onChange(),
    );
    _emailController.addListener(
      () => onChange(),
    );
    super.initState();
  }

  void onChange() {
    _emailController.text.isNotEmpty &&
            passwordRegex.hasMatch(_passwordController.text)
        ? setState(() {
            _btnColor = grape;
            _overlayColor = overlayColor;
          })
        : setState(() {
            _btnColor = mint;
            _overlayColor = Colors.transparent;
          });
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flex(
            crossAxisAlignment: CrossAxisAlignment.start,
            direction: Axis.vertical,
            children: [
              LoginEmailInput(
                controller: _emailController,
              ),
              LoginPasswordInput(
                controller: _passwordController,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: paddingBottom),
            child: Column(
              children: [
                TLButton(
                  name: loginBtn,
                  width: MediaQuery.of(context).size.width,
                  btnColor: _btnColor,
                  borderSideColor: _btnColor,
                  overlayColor: _overlayColor,
                  function: () => _login(context),
                ),
                const ForgotPasswordButton(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _login(BuildContext context) {
    var email = _emailController.text.toLowerCase().trim();
    var password = _passwordController.text.trim();
    if (email.isNotEmpty &&
        passwordRegex.hasMatch(
          password,
        )) {
      BlocProvider.of<LoginBloc>(context).add(
        LoginRequestEvent(
          loginEntity: LoginEntity(
            email: email,
            password: password,
          ),
        ),
      );
    }
  }
}
