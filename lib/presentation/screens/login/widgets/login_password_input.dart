import 'package:flutter/cupertino.dart';

import '/config/tl_strings.dart';
import '/presentation/widgets/tl_text_field.dart';

/// Created by HrAnt
/// Date: 17.06.23

class LoginPasswordInput extends StatelessWidget {
  final TextEditingController controller;

  const LoginPasswordInput({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TLTextField(
      name: 'login_password',
      hint: passwordHint,
      keyboardType: TextInputType.visiblePassword,
      suffixIconVisible: true,
      textInputAction: TextInputAction.done,
      obscureText: true,
      controller: controller,
    );
  }
}
