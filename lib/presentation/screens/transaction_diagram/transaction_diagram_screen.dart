import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/data/enums.dart';
import '/domain/entities/bank/transaction_info.dart';
import '/presentation/bloc/transaction/transaction_bloc.dart';
import '/presentation/bloc/transaction/transaction_event.dart';
import '/presentation/bloc/transaction/transaction_state.dart';
import '/presentation/screens/transaction_diagram/widgets/transaction_list_tile.dart';
import '/presentation/widgets/tl_loading_indicator.dart';
import 'widgets/transaction_diagram.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransactionDiagramScreen extends StatefulWidget {
  const TransactionDiagramScreen({super.key});

  @override
  State<TransactionDiagramScreen> createState() =>
      _TransactionDiagramScreenState();
}

class _TransactionDiagramScreenState extends State<TransactionDiagramScreen> {
  double translationsPercent = 0.0;
  double withdrawalPercent = 0.0;
  double replenishmentPercent = 0.0;

  List<double> transactionsPercents = [];

  @override
  void initState() {
    BlocProvider.of<TransitionBloc>(context).add(
      TransactionsFetchEvent(),
    );
    super.initState();
  }

  void calculateTransactionsPercents(List<TransactionInfoEntity> transactions) {
    translationsPercent = transactions
            .where((element) => element.type == TransactionType.translation)
            .length *
        100 /
        transactions.length;
    withdrawalPercent = transactions
            .where((element) => element.type == TransactionType.withdrawal)
            .length *
        100 /
        transactions.length;
    replenishmentPercent = transactions
            .where((element) => element.type == TransactionType.replenishment)
            .length *
        100 /
        transactions.length;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransitionBloc, TransactionState>(
      builder: (context, state) {
        if (state is TransitionsFetchSuccessfullyState) {
          var transactions = state.transactionInfoEntity;

          calculateTransactionsPercents(transactions);

          transactionsPercents = [
            translationsPercent.roundToDouble(),
            replenishmentPercent.roundToDouble(),
            withdrawalPercent.roundToDouble()
          ];

          return Scaffold(
            backgroundColor: vanillaIce,
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TransactionDiagram(
                    transactionsPercents: transactionsPercents,
                  ),
                  Column(
                    children: transactionsPercents.asMap().entries.map(
                      (e) {
                        int index = e.key;
                        double value = e.value;

                        return TransactionListTile(
                          boxColor: diagramColors[index],
                          title: transactionsTypes[index],
                          percent: value.toString(),
                        );
                      },
                    ).toList(),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container(
            color: vanillaIce,
            child: const TLLoadingIndicator(),
          );
        }
      },
    );
  }
}
