import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransactionListTile extends StatelessWidget {
  final Color boxColor;
  final String title;
  final String percent;

  const TransactionListTile({
    Key? key,
    required this.boxColor,
    required this.title,
    required this.percent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      visualDensity: const VisualDensity(horizontal: 0, vertical: -3),
      horizontalTitleGap: -16.0,
      leading: Container(
        constraints: const BoxConstraints(
          minWidth: 10.0,
          maxWidth: 10.0,
        ),
        child: Align(
          alignment: Alignment.center,
          child: Container(
            color: boxColor,
            height: 10.0,
            width: 10.0,
          ),
        ),
      ),
      title: Align(
        alignment: Alignment.centerLeft,
        child: TLText(
          text: title,
          fontSize: headerSmall,
          fontWeight: FontWeight.w400,
          color: charcoal,
        ),
      ),
      trailing: TLText(
        text: transactions.isNotEmpty ? '$percent%' : '0.0%',
        fontSize: headerSmall,
        fontWeight: FontWeight.w400,
        color: charcoal,
      ),
    );
  }
}
