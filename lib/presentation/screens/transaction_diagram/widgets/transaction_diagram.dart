import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/data/models/diagram_data.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransactionDiagram extends StatelessWidget {
  final List<double> transactionsPercents;

  const TransactionDiagram({
    required this.transactionsPercents,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<DiagramData> diagramData = [];

    if (transactions.isNotEmpty) {
      diagramData = transactionsPercents.asMap().entries.map(
        (e) {
          int index = e.key;
          double value = e.value;

          return DiagramData(
              name: transactionsTypes[index],
              value: value.toInt(),
              color: diagramColors[index]);
        },
      ).toList();
    } else {
      diagramData.add(DiagramData(
        name: '',
        value: 100,
        color: Colors.black,
      ));
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 48.0, horizontal: 28.0),
      child: SfCircularChart(
        series: <CircularSeries>[
          DoughnutSeries<DiagramData, String>(
              animationDuration: 1600,
              animationDelay: 0,
              dataSource: diagramData,
              radius: '120%',
              innerRadius: '70%',
              pointColorMapper: (DiagramData data, _) => data.color,
              xValueMapper: (DiagramData data, _) => data.name,
              yValueMapper: (DiagramData data, _) => data.value)
        ],
        annotations: <CircularChartAnnotation>[
          CircularChartAnnotation(
            horizontalAlignment: ChartAlignment.center,
            verticalAlignment: ChartAlignment.center,
            height: '72.0',
            width: '100.0',
            widget: TLText(
              text: transactions.isNotEmpty
                  ? transactionDiagramTitle
                  : transactionNoFoundTitle,
              fontSize: headerSmall,
              fontWeight: FontWeight.w500,
              color: charcoal,
            ),
          )
        ],
      ),
    );
  }
}
