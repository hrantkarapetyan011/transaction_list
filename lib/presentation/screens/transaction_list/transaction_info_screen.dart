import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/data/enums.dart';
import '/domain/entities/bank/transaction_info.dart';
import '/presentation/bloc/transaction/transaction_bloc.dart';
import '/presentation/bloc/transaction/transaction_event.dart';
import '/presentation/bloc/transaction/transaction_state.dart';
import '/presentation/screens/transaction_list/widgets/info_card.dart';
import '/presentation/widgets/tl_appbar_with_back.dart';
import '/presentation/widgets/tl_button.dart';
import '/presentation/widgets/tl_toast.dart';

/// Created by HrAnt
/// Date: 19.06.23

class TransactionInfoScreen extends StatefulWidget {
  const TransactionInfoScreen({super.key});

  @override
  State<TransactionInfoScreen> createState() => _TransactionInfoScreenState();
}

class _TransactionInfoScreenState extends State<TransactionInfoScreen> {
  int argument = 0;

  @override
  void didChangeDependencies() {
    argument = ModalRoute.of(context)!.settings.arguments as int;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    TransactionInfoEntity? transaction =
        transactions.where((element) => element.number == argument).firstOrNull;

    return BlocListener<TransitionBloc, TransactionState>(
      listener: (context, state) {
        if (state is TransitionCancelSuccessfullyState) {
          showToast(
            context: context,
            title: transactionsDeletedSuccessfully,
            seconds: 1,
          );
          Navigator.pop(context);
          BlocProvider.of<TransitionBloc>(context).add(
            TransactionsFetchEvent(),
          );
        }
        if (state is TransitionCancelFailedState) {
          showToast(
            context: context,
            title: state.error,
            seconds: 3,
            color: lollipop,
          );
        }
      },
      child: BlocBuilder<TransitionBloc, TransactionState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: vanillaIce,
            appBar: TLAppbarWithBack(
              title: '$transactionInfoTitle № ${argument.toString()}',
            ),
            body: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 18.0, vertical: 24.0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    InfoCard(name: 'Date', value: transaction!.date),
                    InfoCard(
                        name: 'Amount',
                        value: '${transaction.amount.toString()} \$'),
                    InfoCard(
                        name: 'Commission',
                        value: '${transaction.commission.toString()} \$'),
                    InfoCard(
                        name: 'Total',
                        value: '${transaction.total.toString()} \$'),
                    InfoCard(
                        name: 'Number', value: transaction.number.toString()),
                    InfoCard(name: 'Type', value: transaction.type.typeTitle),
                    const SizedBox(height: 104.0),
                    TLButton(
                      btnColor: Colors.transparent,
                      textColor: grape,
                      name: cancelTransactionBtn,
                      width: MediaQuery.of(context).size.width,
                      function: () => _cancelTransaction(
                        context,
                        argument,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void _cancelTransaction(BuildContext context, int transactionNumber) {
    BlocProvider.of<TransitionBloc>(context).add(
      TransactionCancelEvent(
        number: transactionNumber,
      ),
    );
  }
}
