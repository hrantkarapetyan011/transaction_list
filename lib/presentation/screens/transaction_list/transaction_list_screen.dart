import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transaction_list/config/tl_strings.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/data/enums.dart';
import '/presentation/bloc/transaction/transaction_bloc.dart';
import '/presentation/bloc/transaction/transaction_event.dart';
import '/presentation/bloc/transaction/transaction_state.dart';
import '/presentation/widgets/tl_loading_indicator.dart';
import '/presentation/widgets/tl_text.dart';
import '/presentation/widgets/tl_toast.dart';
import 'widgets/tl_transaction_card.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransactionListScreen extends StatefulWidget {
  const TransactionListScreen({super.key});

  @override
  State<TransactionListScreen> createState() => _TransactionListScreenState();
}

class _TransactionListScreenState extends State<TransactionListScreen> {
  @override
  void initState() {
    BlocProvider.of<TransitionBloc>(context).add(
      TransactionsFetchEvent(),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<TransitionBloc, TransactionState>(
      listener: (context, state) {
        if (state is TransitionsFetchFailedState) {
          showToast(
            context: context,
            title: state.error,
            seconds: 3,
            color: lollipop,
          );
        }
      },
      child: BlocBuilder<TransitionBloc, TransactionState>(
        builder: (context, state) {
          if (state is TransitionsFetchSuccessfullyState) {
            var transactions = state.transactionInfoEntity.map((transaction) {
              return TLTransactionCard(
                transactionCircleContainerIcon: transaction.type.type,
                transactionNumber: transaction.number.toString(),
                transactionAmount: transaction.amount.toString(),
                transactionType: transaction.type,
              );
            }).toList();

            return SizedBox(
              height: MediaQuery.of(context).size.height,
              child: transactions.isNotEmpty
                  ? ListView.builder(
                      itemCount: state.transactionInfoEntity.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) =>
                          transactions[index],
                    )
                  : const Center(
                      child: TLText(
                        text: transactionNoFoundTitle,
                        fontSize: headerSmall,
                        fontWeight: FontWeight.w400,
                        color: charcoal,
                      ),
                    ),
            );
          } else {
            return Container(
              color: vanillaIce,
              child: const TLLoadingIndicator(),
            );
          }
        },
      ),
    );
  }
}
