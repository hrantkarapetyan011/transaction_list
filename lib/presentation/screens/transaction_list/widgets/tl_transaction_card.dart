import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';
import '/data/enums.dart';
import '/presentation/routes.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TLTransactionCard extends StatelessWidget {
  final IconData transactionCircleContainerIcon;
  final String transactionNumber;
  final String transactionAmount;
  final TransactionType transactionType;

  const TLTransactionCard({
    Key? key,
    required this.transactionCircleContainerIcon,
    required this.transactionNumber,
    required this.transactionAmount,
    required this.transactionType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double mediaSize = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          routeTransactionInfo,
          arguments: int.parse(transactionNumber),
        );
      },
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    Container(
                      width: mediaSize / 14.0,
                      height: mediaSize / 14.0,
                      decoration: BoxDecoration(
                        color: transactionType == TransactionType.translation
                            ? lightBlue
                            : transactionType == TransactionType.withdrawal
                                ? grape
                                : lightGreen,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        transactionCircleContainerIcon,
                        size: 12.0,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 4.0,
                    ),
                    Text(
                      transactionType.typeTitle,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 8.0,
                        color: grape,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 6,
                child: Column(
                  children: [
                    const Text(
                      number,
                      style: TextStyle(
                        fontSize: 10.0,
                        color: grape,
                      ),
                    ),
                    const SizedBox(height: 4.0),
                    Text(
                      transactionNumber,
                      style: const TextStyle(
                        fontSize: 16.0,
                        color: grape,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    const Text(
                      amount,
                      style: TextStyle(
                        fontSize: 10.0,
                        color: grape,
                      ),
                    ),
                    const SizedBox(height: 4.0),
                    Text(
                      '$transactionAmount \$',
                      style: const TextStyle(
                        fontSize: 12.0,
                        color: grape,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
