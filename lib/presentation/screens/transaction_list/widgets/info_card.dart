import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 19.06.23

class InfoCard extends StatelessWidget {
  final String name;
  final String value;

  const InfoCard({
    super.key,
    required this.name,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            children: [
              Expanded(
                child: TLText(
                  text: name,
                  fontSize: headerSmall,
                  fontWeight: FontWeight.w700,
                  color: charcoal,
                  textAlign: TextAlign.left,
                ),
              ),
              Expanded(
                child: TLText(
                  text: value,
                  fontSize: headerSmall,
                  fontWeight: FontWeight.w700,
                  color: charcoal,
                  textAlign: TextAlign.left,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
