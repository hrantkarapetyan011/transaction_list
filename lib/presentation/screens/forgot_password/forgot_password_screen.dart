import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/presentation/widgets/tl_appbar_with_back.dart';
import '/presentation/widgets/tl_text.dart';

/// Created by HrAnt
/// Date: 17.06.23

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: vanillaIce,
      extendBodyBehindAppBar: true,
      appBar: TLAppbarWithBack(
        title: forgotPasswordTitle,
      ),
      body: Center(
        child: TLText(
          text: 'Forgot Password Demo',
          fontSize: headerMedium,
          fontWeight: FontWeight.normal,
          color: charcoal,
        ),
      ),
    );
  }
}
