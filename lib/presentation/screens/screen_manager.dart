import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/presentation/widgets/tl_appbar_with_settings.dart';
import '/presentation/widgets/tl_nav_bar_items.dart';

/// Created by HrAnt
/// Date: 18.06.23

class ScreenManager extends StatefulWidget {
  const ScreenManager({Key? key}) : super(key: key);

  @override
  State<ScreenManager> createState() => _ScreenManagerState();
}

class _ScreenManagerState extends State<ScreenManager> {
  List<BottomNavigationBarItem> get items => getBottomNavBarItems();
  int selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TLAppbarWithSettings(
        title: titles[selectedIndex],
      ),
      backgroundColor: vanillaIce,
      body: pages.elementAt(selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: vanillaIce,
        items: items
            .map<BottomNavigationBarItem>(
              (BottomNavigationBarItem item) => BottomNavigationBarItem(
                icon: item.icon,
                activeIcon: item.activeIcon,
                label: item.label,
              ),
            )
            .toList(),
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: true,
        selectedFontSize: 14.0,
        unselectedFontSize: 14.0,
        iconSize: 24.0,
        unselectedItemColor: charcoal,
        selectedIconTheme: const IconThemeData(color: grape),
        unselectedIconTheme: const IconThemeData(color: charcoal),
        selectedLabelStyle: const TextStyle(
          color: grape,
          fontWeight: FontWeight.w400,
        ),
        unselectedLabelStyle: const TextStyle(
          color: charcoal,
          fontWeight: FontWeight.w400,
        ),
        selectedItemColor: grape,
        elevation: 0,
      ),
    );
  }
}
