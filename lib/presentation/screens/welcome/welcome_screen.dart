import 'package:flutter/material.dart';

import '/config/tl_colors.dart';
import '/config/tl_images.dart';
import '/config/tl_strings.dart';
import '/presentation/routes.dart';
import '/presentation/widgets/privacy_policy.dart';
import '/presentation/widgets/tl_button.dart';
import '/presentation/widgets/tl_skeleton.dart';

/// Created by HrAnt
/// Date: 17.06.23

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TLSkeleton(
      bottomSheetSize: 4,
      imagePath: welcome,
      child: Column(
        children: [
          TLButton(
            name: createAccountBtn,
            width: MediaQuery.of(context).size.width,
            function: () => Navigator.pushNamed(
              context,
              routeSignup,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: TLButton(
              btnColor: Colors.transparent,
              width: MediaQuery.of(context).size.width,
              textColor: grape,
              name: gotAnAccount,
              function: () => Navigator.pushNamed(
                context,
                routeLogin,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 38.0),
            child: PrivacyPolicy(),
          )
        ],
      ),
    );
  }
}
