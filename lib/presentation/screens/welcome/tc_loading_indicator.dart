import 'package:flutter/material.dart';

import '/config/tl_colors.dart';

/// Created by HrAnt
/// Date: 18.06.23

class HCLoadingIndicator extends StatelessWidget {
  final bool withRadius;

  const HCLoadingIndicator({
    Key? key,
    this.withRadius = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: withRadius ? null : const Color.fromRGBO(0, 0, 0, 0.6),
      decoration: withRadius
          ? const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(24.0)),
              color: Color.fromRGBO(0, 0, 0, 0.6),
            )
          : null,
      child: const Align(
        alignment: Alignment.center,
        child: SizedBox(
          height: 64.0,
          width: 64.0,
          child: CircularProgressIndicator(
            strokeWidth: 6.0,
            color: grape,
          ),
        ),
      ),
    );
  }
}
