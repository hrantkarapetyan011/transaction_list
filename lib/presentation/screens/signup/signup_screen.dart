import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transaction_list/presentation/bloc/register/register_bloc.dart';
import 'package:transaction_list/presentation/bloc/register/register_state.dart';
import 'package:transaction_list/presentation/widgets/tl_toast.dart';

import '/config/tl_colors.dart';
import '/config/tl_strings.dart';
import '/presentation/screens/signup/widgets/signup_form.dart';
import '/presentation/widgets/tl_appbar_with_back.dart';
import '/presentation/widgets/tl_screen_description.dart';

/// Created by HrAnt
/// Date: 17.06.23

class SignupScreen extends StatelessWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterSuccessfullyState) {
          showToast(
            context: context,
            title: signupSuccessfully,
            seconds: 3,
          );
          Navigator.pop(context);
        }
        if (state is RegisterFailedState) {
          showToast(
            context: context,
            title: state.error,
            seconds: 3,
            color: lollipop,
          );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return const Scaffold(
            backgroundColor: vanillaIce,
            resizeToAvoidBottomInset: false,
            appBar: TLAppbarWithBack(
              title: signUpTitle,
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TLScreenDescription(
                    title: signupScreenTitle,
                    subtitle: signupScreenSubtitle,
                  ),
                  SignupForm(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
