import 'package:flutter/cupertino.dart';

import '/config/tl_strings.dart';
import '/presentation/widgets/tl_text_field.dart';

/// Created by HrAnt
/// Date: 17.06.23

class SignupNameInput extends StatelessWidget {
  final TextEditingController controller;

  const SignupNameInput({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TLTextField(
      name: 'signup_name',
      hint: nameHint,
      keyboardType: TextInputType.name,
      textInputAction: TextInputAction.next,
      controller: controller,
    );
  }
}
