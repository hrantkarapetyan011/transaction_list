import 'package:flutter/cupertino.dart';

import '/config/tl_strings.dart';
import '/presentation/widgets/tl_text_field.dart';

/// Created by HrAnt
/// Date: 17.06.23

class SignupEmailInput extends StatelessWidget {
  final TextEditingController controller;

  const SignupEmailInput({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TLTextField(
      name: 'signup_email',
      hint: emailHint,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      controller: controller,
    );
  }
}
