import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/config/tl_colors.dart';
import '/config/tl_constants.dart';
import '/config/tl_strings.dart';
import '/domain/entities/auth/register.dart';
import '/presentation/bloc/register/register_bloc.dart';
import '/presentation/bloc/register/register_event.dart';
import '/presentation/screens/signup/widgets/signup_email_input.dart';
import '/presentation/screens/signup/widgets/signup_name_input.dart';
import '/presentation/screens/signup/widgets/signup_password_input.dart';
import '/presentation/widgets/tl_button.dart';

/// Created by HrAnt
/// Date: 17.06.23

class SignupForm extends StatefulWidget {
  const SignupForm({Key? key}) : super(key: key);

  @override
  State<SignupForm> createState() => _SignupFormFormState();
}

class _SignupFormFormState extends State<SignupForm> {
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  Color _btnColor = mint;
  Color _overlayColor = Colors.transparent;

  @override
  void initState() {
    _passwordController.addListener(
      () => onChange(),
    );
    _emailController.addListener(
      () => onChange(),
    );
    _nameController.addListener(
      () => onChange(),
    );
    super.initState();
  }

  void onChange() {
    _emailController.text.isNotEmpty &&
            _nameController.text.isNotEmpty &&
            passwordRegex.hasMatch(_passwordController.text)
        ? setState(() {
            _btnColor = grape;
            _overlayColor = overlayColor;
          })
        : setState(() {
            _btnColor = mint;
            _overlayColor = Colors.transparent;
          });
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _emailController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flex(
            crossAxisAlignment: CrossAxisAlignment.start,
            direction: Axis.vertical,
            children: [
              SignupNameInput(
                controller: _nameController,
              ),
              SignupEmailInput(
                controller: _emailController,
              ),
              SignupPasswordInput(
                controller: _passwordController,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: paddingBottom),
            child: Column(
              children: [
                TLButton(
                  name: signUpBtn,
                  width: MediaQuery.of(context).size.width,
                  btnColor: _btnColor,
                  borderSideColor: _btnColor,
                  overlayColor: _overlayColor,
                  function: () => _signup(context),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _signup(BuildContext context) {
    var email = _emailController.text.toLowerCase().trim();
    var password = _passwordController.text.trim();
    var name = _nameController.text.trim();
    if (email.isNotEmpty &&
        name.isNotEmpty &&
        passwordRegex.hasMatch(
          password,
        )) {
      BlocProvider.of<RegisterBloc>(context).add(
        RegisterDataEvent(
          registerEntity: RegisterEntity(
            email: email,
            password: password,
            name: name,
          ),
        ),
      );
    }
  }
}
