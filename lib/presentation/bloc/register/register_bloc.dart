import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:transaction_list/config/tl_strings.dart';

import '/domain/repositories/auth_repository.dart';
import 'register_event.dart';
import 'register_state.dart';

/// Created by HrAnt
/// Date: 18.06.23

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository authRepository;

  RegisterBloc({required this.authRepository}) : super(NotRegisteredState()) {
    on<RegisterDataEvent>(_register);
  }

  FutureOr<void> _register(
    RegisterDataEvent event,
    Emitter<RegisterState> emit,
  ) async {
    try {
      emit(
        LoadingState(),
      );

      final res = await authRepository.register(
        event.registerEntity,
      );

      if (res) {
        emit(
          RegisterSuccessfullyState(),
        );
      } else {
        emit(RegisterFailedState(error: somethingWentWrong));
        emit(NotRegisteredState());
      }
    } catch (e) {
      emit(
        RegisterFailedState(
          error: e.toString(),
        ),
      );
      emit(NotRegisteredState());
    }
  }
}
