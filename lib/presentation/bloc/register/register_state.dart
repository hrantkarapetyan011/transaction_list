import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '/domain/entities/auth/register.dart';

/// Created by HrAnt
/// Date: 18.06.23

@immutable
abstract class RegisterState extends Equatable {}

class LoadingState extends RegisterState {
  @override
  List<Object?> get props => [];
}

class NotRegisteredState extends RegisterState {
  @override
  List<Object?> get props => [];
}

class RegisteringState extends RegisterState {
  final RegisterEntity registerEntity;

  RegisteringState({
    required this.registerEntity,
  });

  @override
  List<Object?> get props => [registerEntity];
}

class RegisterSuccessfullyState extends RegisterState {
  @override
  List<Object?> get props => [];
}

class RegisterFailedState extends RegisterState {
  final String error;

  RegisterFailedState({
    required this.error,
  });

  @override
  List<Object?> get props => [error];
}
