import 'package:equatable/equatable.dart';

import '/domain/entities/auth/register.dart';

/// Created by HrAnt
/// Date: 18.06.23

abstract class RegisterEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RegisterDataEvent extends RegisterEvent {
  final RegisterEntity registerEntity;

  RegisterDataEvent({required this.registerEntity});

  @override
  List<Object> get props => [registerEntity];
}
