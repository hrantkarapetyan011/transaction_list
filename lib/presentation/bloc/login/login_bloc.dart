import 'dart:async';

import 'package:bloc/bloc.dart';

import '/config/tl_strings.dart';
import '/data/exceptions.dart';
import '/data/sources/local_sources/shared_preferences_manager.dart';
import '/domain/repositories/auth_repository.dart';
import 'login_event.dart';
import 'login_state.dart';

/// Created by HrAnt
/// Date: 18.06.23

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({required this.authRepository}) : super(LoginProcessState()) {
    on<LoginRequestEvent>(_login);
  }

  final AuthRepository authRepository;

  FutureOr<void> _login(
    LoginRequestEvent event,
    Emitter<LoginState> emit,
  ) async {
    try {
      emit(LoadingState());

      final res = await authRepository.login(
        event.loginEntity,
      );

      if (res.data?.token != null) {
        await sharedPreferences!
            .setString(accessToken, res.data?.token ?? '')
            .then(
          (value) {
            emit(
              LoginSuccessfullyState(),
            );
          },
        );
      } else {
        emit(LoginFailedState(error: res.message!));
        emit(LoginProcessState());
      }
    } on LoginOrPasswordWrongException catch (e) {
      emit(
        LoginFailedState(
          error: e.message,
        ),
      );
    } catch (e) {
      emit(LoginProcessState());
    }
  }
}
