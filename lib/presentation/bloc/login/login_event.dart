import 'package:equatable/equatable.dart';
import 'package:transaction_list/domain/entities/auth/login.dart';

/// Created by HrAnt
/// Date: 18.06.23

abstract class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginRequestEvent extends LoginEvent {
  final LoginEntity loginEntity;

  LoginRequestEvent({
    required this.loginEntity,
  });

  @override
  List<Object> get props => [loginEntity];
}
