import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

/// Created by HrAnt
/// Date: 18.06.23

@immutable
abstract class LoginState extends Equatable {}

class LoadingState extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginSuccessfullyState extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginProcessState extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginFailedState extends LoginState {
  final String error;

  LoginFailedState({
    required this.error,
  });

  @override
  List<Object?> get props => [error];
}
