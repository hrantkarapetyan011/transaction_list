import 'package:equatable/equatable.dart';

/// Created by HrAnt
/// Date: 18.06.23

abstract class TransactionEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class TransactionsFetchEvent extends TransactionEvent {}

class TransactionCancelEvent extends TransactionEvent {
  final int number;

  TransactionCancelEvent({
    required this.number,
  });

  @override
  List<Object> get props => [number];
}
