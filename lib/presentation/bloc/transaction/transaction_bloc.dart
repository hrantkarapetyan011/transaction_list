import 'dart:async';

import 'package:bloc/bloc.dart';

import '/domain/repositories/transaction_repository.dart';
import '/presentation/bloc/transaction/transaction_event.dart';
import '/presentation/bloc/transaction/transaction_state.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransitionBloc extends Bloc<TransactionEvent, TransactionState> {
  final TransactionRepository transactionRepository;

  TransitionBloc({required this.transactionRepository})
      : super(TransitionInitialState()) {
    on<TransactionsFetchEvent>(_fetchTransactions);
    on<TransactionCancelEvent>(_cancelTransaction);
  }

  FutureOr<void> _fetchTransactions(
    TransactionsFetchEvent event,
    Emitter<TransactionState> emit,
  ) async {
    try {
      emit(
        LoadingState(),
      );

      final res = await transactionRepository.fetchTransactions();

      emit(
        TransitionsFetchSuccessfullyState(
          transactionInfoEntity: res,
        ),
      );
    } catch (e) {
      emit(
        TransitionsFetchFailedState(
          error: e.toString(),
        ),
      );
      emit(TransitionInitialState());
    }
  }

  FutureOr<void> _cancelTransaction(
    TransactionCancelEvent event,
    Emitter<TransactionState> emit,
  ) async {
    try {
      emit(
        LoadingState(),
      );

      await transactionRepository.cancelTransaction(event.number);

      emit(
        TransitionCancelSuccessfullyState(),
      );
    } catch (e) {
      emit(
        TransitionCancelFailedState(
          error: e.toString(),
        ),
      );
    }
  }
}
