import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '/domain/entities/bank/transaction_info.dart';

/// Created by HrAnt
/// Date: 18.06.23

@immutable
abstract class TransactionState extends Equatable {}

class LoadingState extends TransactionState {
  @override
  List<Object?> get props => [];
}

class TransitionInitialState extends TransactionState {
  @override
  List<Object?> get props => [];
}

class TransitionsFetchState extends TransactionState {
  final List<TransactionInfoEntity> transactionInfoEntity;

  TransitionsFetchState({
    required this.transactionInfoEntity,
  });

  @override
  List<Object?> get props => [transactionInfoEntity];
}

class TransitionsFetchSuccessfullyState extends TransactionState {
  final List<TransactionInfoEntity> transactionInfoEntity;

  TransitionsFetchSuccessfullyState({
    required this.transactionInfoEntity,
  });

  @override
  List<Object?> get props => [transactionInfoEntity];
}

class TransitionsFetchFailedState extends TransactionState {
  final String error;

  TransitionsFetchFailedState({
    required this.error,
  });

  @override
  List<Object?> get props => [error];
}

class TransitionCancelSuccessfullyState extends TransactionState {
  @override
  List<Object?> get props => [];
}

class TransitionCancelFailedState extends TransactionState {
  final String error;

  TransitionCancelFailedState({
    required this.error,
  });

  @override
  List<Object?> get props => [error];
}
