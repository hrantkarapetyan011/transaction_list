import 'package:flutter/material.dart';

import '/presentation/screens/forgot_password/forgot_password_screen.dart';
import '/presentation/screens/login/login_screen.dart';
import '/presentation/screens/screen_manager.dart';
import '/presentation/screens/settings/settings_screen.dart';
import '/presentation/screens/signup/signup_screen.dart';
import '/presentation/screens/transaction_list/transaction_info_screen.dart';
import '/presentation/screens/welcome/welcome_screen.dart';

/// Created by HrAnt
/// Date: 17.06.23

const String routeWelcome = '/welcome';

const String routeLogin = '/login';
const String routeForgotPassword = '/forgot_password';

const String routeSignup = '/signup';

const String routeScreenManager = '/screen_manager';

const String routeTransactionInfo = '/transaction_info';

const String routeSettings = '/settings';

typedef ScreenBuilder = WidgetBuilder Function(
  RouteSettings settings,
  RouteSettings? previousSettings,
);

Map<String, ScreenBuilder> routes = <String, ScreenBuilder>{
  routeWelcome: (RouteSettings settings, RouteSettings? previousSettings) =>
      (BuildContext context) => const WelcomeScreen(),
  routeLogin: (RouteSettings settings, RouteSettings? previousSettings) =>
      (BuildContext context) => const LoginScreen(),
  routeSignup: (RouteSettings settings, RouteSettings? previousSettings) =>
      (BuildContext context) => const SignupScreen(),
  routeForgotPassword:
      (RouteSettings settings, RouteSettings? previousSettings) =>
          (BuildContext context) => const ForgotPasswordScreen(),
  routeScreenManager:
      (RouteSettings settings, RouteSettings? previousSettings) =>
          (BuildContext context) => const ScreenManager(),
  routeTransactionInfo:
      (RouteSettings settings, RouteSettings? previousSettings) =>
          (BuildContext context) => const TransactionInfoScreen(),
  routeSettings: (RouteSettings settings, RouteSettings? previousSettings) =>
      (BuildContext context) => const SettingsScreen(),
};

class HCPageRoute extends PageRouteBuilder<Widget> {
  HCPageRoute({
    required WidgetBuilder widgetBuilder,
    required RouteSettings routeSettings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(-1, 0),
                end: Offset.zero,
              ).animate(
                CurvedAnimation(
                  curve: const Interval(
                    0,
                    0,
                    curve: Curves.easeIn,
                  ),
                  parent: animation,
                ),
              ),
              child: widgetBuilder(context),
            );
          },
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
          settings: routeSettings,
        );

  final PageTransitionsBuilder matchingBuilder =
      const CupertinoPageTransitionsBuilder();

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return matchingBuilder.buildTransitions(
        this, context, animation, secondaryAnimation, child);
  }
}
