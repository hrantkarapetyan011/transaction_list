import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import 'app_manager.dart';
import 'config/tl_strings.dart';
import 'data/sources/local_sources/shared_preferences_manager.dart';
import 'firebase_options.dart';
import 'injection.dart';

/// Created by HrAnt
/// Date: 16.06.23

Future<void> main() async {
  final WidgetsBinding widgetsBinding =
      WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  /// Load the .env file
  await dotenv.load(fileName: '.env');

  /// set only portrait mode
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  /// init Flutter Native Splash Screen
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  /// init service locator for DI
  await initLocator();

  await setSharedPreferencesInstance();

  /// Equatable can implement toString method including all the given props
  /// Equatable stringify configured globally if [EquatableConfig.stringify = true];
  EquatableConfig.stringify = true;

  final hasToken = sharedPreferences!.containsKey(
    accessToken,
  );

  runApp(
    TransactionListAppManager(
      hasToken: hasToken,
    ),
  );
}
