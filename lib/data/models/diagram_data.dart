import 'package:flutter/material.dart';

/// Created by HrAnt
/// Date: 18.06.23

class DiagramData {
  DiagramData({
    required this.name,
    required this.value,
    required this.color,
  });

  final String name;
  final int value;
  final Color color;
}
