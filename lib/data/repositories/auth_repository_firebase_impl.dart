import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

import '/config/tl_strings.dart';
import '/domain/entities/auth/login.dart';
import '/domain/entities/auth/login_response.dart';
import '/domain/entities/auth/register.dart';
import '/domain/entities/token/token.dart';
import '/domain/repositories/auth_repository.dart';

/// Created by HrAnt
/// Date: 18.06.23

class AuthRepositoryFirebaseImpl extends AuthRepository {
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  Future<bool> register(
    RegisterEntity registerEntity,
  ) async {
    try {
      FirebaseAuth.instance
          .createUserWithEmailAndPassword(
            email: registerEntity.email!,
            password: registerEntity.password!,
          )
          .then((userCredentials) => {
                users.add({
                  'full_name': registerEntity.name,
                  'aut_id': userCredentials.user?.uid,
                  'email': userCredentials.user?.email,
                }),
              });
      return true;
    } catch (err) {
      if (kDebugMode) {
        print(err.toString());
      }
      return false;
    }
  }

  @override
  Future<LoginResponseEntity> login(
    LoginEntity loginEntity,
  ) async {
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: loginEntity.email,
        password: loginEntity.password,
      );

      /// Free version of Firebase doesn't return token,
      /// that's why I use UID instead of token
      return LoginResponseEntity(
          data: TokenEntity(token: userCredential.user?.uid), message: null);
    } on FirebaseAuthException catch (e) {
      if (e.credential != null) {
        if (kDebugMode) {
          print(e.message);
        }
      }
      return LoginResponseEntity(
        data: null,
        message: loginError,
      );
    }
  }
}
