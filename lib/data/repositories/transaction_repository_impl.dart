import '/config/tl_constants.dart';
import '/domain/entities/bank/transaction_info.dart';
import '/domain/repositories/transaction_repository.dart';

/// Created by HrAnt
/// Date: 18.06.23

class TransactionRepositoryImpl extends TransactionRepository {
  @override
  Future<List<TransactionInfoEntity>> fetchTransactions() async {
    return transactions;
  }

  @override
  Future<void> cancelTransaction(int number) async {
    transactions.removeWhere(
      (element) => element.number == number,
    );
  }
}
