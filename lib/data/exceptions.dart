/// Created by HrAnt
/// Date: 17.06.23

class LoginOrPasswordWrongException implements Exception {
  final String message;

  LoginOrPasswordWrongException({
    required this.message,
  });
}
