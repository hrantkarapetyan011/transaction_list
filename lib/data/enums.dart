import 'package:flutter/material.dart';

import '/config/tl_strings.dart';

/// Created by HrAnt
/// Date: 18.06.23

enum TransactionType {
  translation,
  replenishment,
  withdrawal,
}

extension TLType on TransactionType {
  IconData get type {
    switch (this) {
      case TransactionType.translation:
        return Icons.arrow_upward_outlined;
      case TransactionType.replenishment:
        return Icons.monetization_on;
      case TransactionType.withdrawal:
        return Icons.arrow_downward_outlined;
    }
  }
}

extension TLTypeTitle on TransactionType {
  String get typeTitle {
    switch (this) {
      case TransactionType.translation:
        return translationType;
      case TransactionType.replenishment:
        return replenishmentType;
      case TransactionType.withdrawal:
        return withdrawalType;
    }
  }
}
