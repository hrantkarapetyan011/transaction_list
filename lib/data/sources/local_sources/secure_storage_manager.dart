import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Created by HrAnt
/// Date: 17.06.23

class SecureStorageManager {
  final FlutterSecureStorage secureStorage;

  SecureStorageManager({
    required this.secureStorage,
  });
}
