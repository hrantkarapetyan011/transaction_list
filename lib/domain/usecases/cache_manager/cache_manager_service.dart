import 'package:flutter/cupertino.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

/// See in [LINK] 'https://pub.dev/packages/flutter_cache_manager#breaking-changes-in-v2'.
///
/// * [stalePeriod] is the time period during which the cache object is considered "stale".
/// When a file is cached but not used for a certain amount of time, the file will be deleted.
///
/// * [maxNrOfCacheObjects] determines how big the cache can be.
/// If there are more files, the files that have not been used the longest will be deleted.

/// Created by HrAnt
/// Date: 18.06.23

final CacheManager customCacheManager = CacheManager(
  Config(
    'customCacheKey',
    maxNrOfCacheObjects: 100,
  ),
);

Future<void> clearCache() async {
  await customCacheManager.emptyCache();
  imageCache.clear();
  imageCache.clearLiveImages();
}
