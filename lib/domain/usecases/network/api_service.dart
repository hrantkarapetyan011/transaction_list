import 'package:dio/dio.dart';

import '/config/tl_strings.dart';
import '/data/sources/local_sources/shared_preferences_manager.dart';

/// Created by HrAnt
/// Date: 18.06.23

final BaseOptions options = BaseOptions(
  contentType: Headers.jsonContentType,
  connectTimeout: const Duration(seconds: 5),
  receiveTimeout: const Duration(seconds: 5),
);

class HCInterceptors extends Interceptor {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final String? token = sharedPreferences!.getString(accessToken);
    options.headers[authorization] = 'Bearer $token';
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // TODO: implement onResponse
    super.onResponse(response, handler);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    // TODO: implement onError
    super.onError(err, handler);
  }
}

class DioClient {
  final Dio _dio = Dio(options);

  DioClient() {
    _dio.interceptors.add(
      HCInterceptors(),
    );
  }

  Dio get dio => _dio;
}
