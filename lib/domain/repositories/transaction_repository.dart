import '/domain/entities/bank/transaction_info.dart';

/// Created by HrAnt
/// Date: 18.06.23

abstract class TransactionRepository {
  Future<List<TransactionInfoEntity>> fetchTransactions();

  Future<void> cancelTransaction(int number);
}
