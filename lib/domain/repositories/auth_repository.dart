import '/domain/entities/auth/login.dart';
import '/domain/entities/auth/login_response.dart';
import '/domain/entities/auth/register.dart';

/// Created by HrAnt
/// Date: 18.06.23

abstract class AuthRepository {
  Future<bool> register(
    RegisterEntity registerEntity,
  );

  Future<LoginResponseEntity> login(
    LoginEntity loginEntity,
  );
}
