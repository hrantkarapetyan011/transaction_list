import 'package:json_annotation/json_annotation.dart';

import '/data/enums.dart';

/// Created by HrAnt
/// Date: 18.06.23

part 'transaction_info.g.dart';

@JsonSerializable()
class TransactionInfoEntity {
  final String date;
  final double commission;
  final double total;
  final TransactionType type;
  final int number;
  final double amount;

  TransactionInfoEntity({
    required this.date,
    required this.commission,
    required this.total,
    required this.type,
    required this.number,
    required this.amount,
  });

  factory TransactionInfoEntity.fromJson(Map<String, dynamic> map) =>
      _$TransactionInfoEntityFromJson(map);

  Map<String, dynamic> toJson() => _$TransactionInfoEntityToJson(this);
}
