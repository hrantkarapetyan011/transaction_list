// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionInfoEntity _$TransactionInfoEntityFromJson(
        Map<String, dynamic> json) =>
    TransactionInfoEntity(
      date: json['date'] as String,
      commission: (json['commission'] as num).toDouble(),
      total: (json['total'] as num).toDouble(),
      type: $enumDecode(_$TransactionTypeEnumMap, json['type']),
      number: json['number'] as int,
      amount: (json['amount'] as num).toDouble(),
    );

Map<String, dynamic> _$TransactionInfoEntityToJson(
        TransactionInfoEntity instance) =>
    <String, dynamic>{
      'date': instance.date,
      'commission': instance.commission,
      'total': instance.total,
      'type': _$TransactionTypeEnumMap[instance.type]!,
      'number': instance.number,
      'amount': instance.amount,
    };

const _$TransactionTypeEnumMap = {
  TransactionType.translation: 'translation',
  TransactionType.replenishment: 'replenishment',
  TransactionType.withdrawal: 'withdrawal',
};
