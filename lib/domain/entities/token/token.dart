import 'package:json_annotation/json_annotation.dart';

/// Created by HrAnt
/// Date: 18.06.23

part 'token.g.dart';

@JsonSerializable()
class TokenEntity {
  final String? token;

  TokenEntity({
    required this.token,
  });

  factory TokenEntity.fromJson(Map<String, dynamic> map) =>
      _$TokenEntityFromJson(map);

  Map<String, dynamic> toJson() => _$TokenEntityToJson(this);
}
