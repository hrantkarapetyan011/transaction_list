import 'package:json_annotation/json_annotation.dart';

/// Created by HrAnt
/// Date: 18.06.23

part 'register.g.dart';

@JsonSerializable()
class RegisterEntity {
  String? name;
  String? email;
  String? password;

  RegisterEntity({
    required this.name,
    required this.email,
    required this.password,
  });

  factory RegisterEntity.fromJson(Map<String, dynamic> map) =>
      _$RegisterEntityFromJson(map);

  Map<String, dynamic> toJson() => _$RegisterEntityToJson(this);
}
