import 'package:json_annotation/json_annotation.dart';

import '/domain/entities/token/token.dart';

/// Created by HrAnt
/// Date: 18.06.23

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponseEntity {
  final TokenEntity? data;
  final String? message;

  LoginResponseEntity({
    required this.data,
    required this.message,
  });

  factory LoginResponseEntity.fromJson(Map<String, dynamic> map) =>
      _$LoginResponseEntityFromJson(map);

  Map<String, dynamic> toJson() => _$LoginResponseEntityToJson(this);
}
