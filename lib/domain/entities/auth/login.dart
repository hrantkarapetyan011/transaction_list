import 'package:json_annotation/json_annotation.dart';

/// Created by HrAnt
/// Date: 18.06.23

part 'login.g.dart';

@JsonSerializable()
class LoginEntity {
  final String email;
  final String password;

  LoginEntity({
    required this.email,
    required this.password,
  });

  factory LoginEntity.fromJson(Map<String, dynamic> map) =>
      _$LoginEntityFromJson(map);

  Map<String, dynamic> toJson() => _$LoginEntityToJson(this);
}
