import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import '/data/repositories/auth_repository_firebase_impl.dart';
import '/data/repositories/transaction_repository_impl.dart';
import '/domain/repositories/transaction_repository.dart';
import '/presentation/bloc/login/login_bloc.dart';
import '/presentation/bloc/register/register_bloc.dart';
import '/presentation/bloc/transaction/transaction_bloc.dart';
import '/presentation/routes.dart';
import 'config/tl_fonts.dart';
import 'domain/repositories/auth_repository.dart';

/// Created by HrAnt
/// Date: 16.06.23

class TransactionListAppManager extends StatefulWidget {
  final bool hasToken;

  const TransactionListAppManager({
    super.key,
    required this.hasToken,
  });

  @override
  State<TransactionListAppManager> createState() =>
      _TransactionListAppManagerState();
}

class _TransactionListAppManagerState extends State<TransactionListAppManager>
    with WidgetsBindingObserver {
  String initialRoute = routeWelcome;

  RouteSettings? previousSettings;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    initSplash();
  }

  /// This sample shows how to implement parts of the [State] and
  /// [WidgetsBindingObserver] protocols necessary to react to application
  /// lifecycle messages. See [didChangeAppLifecycleState].
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
      case AppLifecycleState.hidden:
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  /// Flutter Native Splash Screen work function
  Future<void> initSplash() async {
    Future<dynamic>.delayed(
      const Duration(
        milliseconds: 3000,
      ),
      () {
        FlutterNativeSplash.remove();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AuthRepository>(
            create: (context) => AuthRepositoryFirebaseImpl(),
          ),
          RepositoryProvider<TransactionRepository>(
            create: (context) => TransactionRepositoryImpl(),
          ),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider<RegisterBloc>(
              create: (context) => RegisterBloc(
                authRepository: RepositoryProvider.of<AuthRepository>(
                  context,
                ),
              ),
            ),
            BlocProvider<LoginBloc>(
              create: (context) => LoginBloc(
                authRepository: RepositoryProvider.of<AuthRepository>(
                  context,
                ),
              ),
            ),
            BlocProvider<TransitionBloc>(
              create: (context) => TransitionBloc(
                transactionRepository:
                    RepositoryProvider.of<TransactionRepository>(
                  context,
                ),
              ),
            ),
          ],
          child: MaterialApp(
            theme: ThemeData(
              fontFamily: sfProDisplayRegular,
            ),
            onGenerateRoute: (RouteSettings settings) {
              final bool contains = routes.keys.contains(settings.name);

              SchedulerBinding.instance.addPostFrameCallback(
                (Duration timestamp) {
                  previousSettings = settings;
                },
              );

              if (widget.hasToken) {
                initialRoute = routeScreenManager;
              }

              return HCPageRoute(
                widgetBuilder: contains
                    ? routes[settings.name!]!(settings, previousSettings)
                    : routes[initialRoute]!(settings, previousSettings),
                routeSettings: settings,
              );
            },
          ),
        ),
      ),
    );
  }
}
